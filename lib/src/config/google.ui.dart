import 'dart:ui';

import 'package:flutter/rendering.dart';

class MaterialRadius {
  static const card = 8.0;
  static const dialog = 4.0;
}

class MaterialSpace {
  static const medium = 24.0;
  static const narrow = 16.0;
  static const tiny = 8.0;
}

class GoogleColor {
  static const caption = Color(0xff5f6368);
  static const inactive_object = Color(0xff595959);
  static const border = Color(0xffdadce0);
}

class GoogleInset {

  static const header_tiny = EdgeInsets.only(
    top: MaterialSpace.medium, bottom: MaterialSpace.tiny,
    left: MaterialSpace.medium, right: MaterialSpace.medium
  );

  static const header_narrow = EdgeInsets.only(
    top: MaterialSpace.medium, bottom: MaterialSpace.narrow,
    left: MaterialSpace.medium, right: MaterialSpace.medium
  );

  static const narrow = EdgeInsets.symmetric(
    vertical: MaterialSpace.narrow, horizontal: MaterialSpace.medium
  );

}