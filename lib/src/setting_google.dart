import 'dart:async';

import 'package:flutter/material.dart';
import 'config/google.ui.dart';
import 'config/color_states.dart';


class GoogleSettingHint extends StatelessWidget {
  final String text;

  const GoogleSettingHint({Key key, this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(text.toUpperCase(),
      softWrap: true,
      style: Theme.of(context).textTheme.caption.apply(
        color: GoogleColor.caption,
        fontWeightDelta: 500
      ),
    );
  }
}


class GoogleSettingField extends StatefulWidget {

  final Color hoverColor;
  final String hint;
  final Widget Function() childBuilder;
  final Function onTap;
  final int titleFlex;
  final Icon icon;

  const GoogleSettingField({Key key,
    @required this.childBuilder,
    this.hint,
    this.hoverColor,
    this.onTap,
    this.titleFlex = 4,
    this.icon,
  }): assert(childBuilder != null),
      super(key: key);

  @override
  _GoogleSettingFieldState createState() => _GoogleSettingFieldState();
}

class _GoogleSettingFieldState extends State<GoogleSettingField> {

  StreamController<bool> _updateController;

  @override
  void initState() {
    _updateController = StreamController();
    super.initState();
  }

  @override
  void dispose() {
    _updateController.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final color = widget.hoverColor ?? Theme.of(context).accentColor;
    return Material(
      type: MaterialType.transparency,
      child: InkWell(
        hoverColor: color.hover,
        splashColor: color.pressed,
        focusColor: color.focus,
        highlightColor: color.activated,
        onTap: () {
          Future.microtask(widget.onTap)
            .then((value) => _updateController.sink.add(true));
        },
        child: Container(
          padding: GoogleInset.narrow,
          child: Row(
            children: <Widget>[
              Expanded(
                flex: widget.titleFlex,
                child: GoogleSettingHint(
                  text: widget.hint,
                )
              ),
              Expanded(
                flex: 10,
                child: StreamBuilder<bool>(
                  stream: _updateController.stream,
                  builder: (context, snapshot) => widget.childBuilder()
                )
              ),
              if (widget.icon != null)
                Expanded(
                  child: Container(
                    alignment: Alignment.center,
                    child: Align(
                      child: widget.icon
                    ),
                  ),
                )
            ],
          ),
        ),
      ),
    );
  }
}


class GoogleSettingArea extends StatelessWidget {
  final String title;
  final String description;
  final Widget child;

  const GoogleSettingArea({Key key,
    this.title,
    this.description,
    @required this.child
  }): assert(child != null),
      super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAliasWithSaveLayer,
      margin: EdgeInsets.all(MaterialSpace.tiny),
      decoration: BoxDecoration(
        border: Border.all(
          color: GoogleColor.border,
        ),
        borderRadius: BorderRadius.circular(MaterialRadius.card),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Padding(
            padding: GoogleInset.header_tiny,
            child: Text(title,
              style: Theme.of(context).textTheme.headline5,
            ),
          ),
          child
        ],
      ),
    );
  }
}

class GoogleSettingDivider extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: MaterialSpace.medium),
      child: Divider(
        thickness: 1, height: 1, color: GoogleColor.border,
      ),
    );
  }
}


class GoogleDialog extends StatelessWidget {

  final String title;
  final List<Widget> children;
  final Function onDiscard;
  final Function onAccept;
  final Function feedbackBuilder;
  final double width;

  const GoogleDialog({Key key,
    this.title = "",
    this.children,
    @required this.onAccept,
    @required this.onDiscard,
    this.feedbackBuilder,
    this.width = 400,
  }): assert(onAccept != null),
      assert(onDiscard != null),
      super(key: key);

  _close(context) {
    onDiscard();
    if (Navigator.of(context).canPop())
      Navigator.of(context).pop();
  }

  _save(context) {
    onAccept();
    if (Navigator.of(context).canPop() && feedbackBuilder != null)
      Navigator.of(context).pop(feedbackBuilder());
  }

  @override
  Widget build(BuildContext context) {
    final color = Theme.of(context).accentColor;
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(MaterialRadius.card)
      ), //this right here
      child: Container(
        width: width,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              padding: GoogleInset.header_narrow,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(right: MaterialSpace.narrow),
                    child: Text(title,
                      style: Theme.of(context).textTheme.headline6.apply(
                        fontWeightDelta: 500,
                      )),
                  ),
                  IconButton(
                    icon: Icon(Icons.close),
                    onPressed: () => _close(context),
                  )
                ],
              ),
            ),
            if (children != null)
              Padding(
                padding: EdgeInsets.symmetric(
                  horizontal: MaterialSpace.medium
                ),
                child: Column(
                  children: children,
                ),
              ),
            Container(
              padding: GoogleInset.header_narrow,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  FlatButton(
                    hoverColor: GoogleColor.inactive_object.hover,
                    focusColor: GoogleColor.inactive_object.focus,
                    highlightColor: GoogleColor.inactive_object.activated,
                    splashColor: GoogleColor.inactive_object.pressed,
                    onPressed: () => _close(context),
                    child: Text("Отменить".toUpperCase(),
                      style: Theme.of(context).textTheme.button.apply(
                        color: GoogleColor.inactive_object,
                        fontWeightDelta: 500
                      ),
                    ),
                  ),
                  Container(width: MaterialSpace.tiny),
                  FlatButton(
                    hoverColor: color.hover,
                    focusColor: color.focus,
                    highlightColor: color.activated,
                    splashColor: color.pressed,
                    onPressed: () => _save(context),
                    child: Text("Сохранить".toUpperCase(),
                      style: Theme.of(context).textTheme.button.apply(
                        color: color,
                        fontWeightDelta: 500
                      ),
                    ),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}
